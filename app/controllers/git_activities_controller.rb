class GitActivitiesController < ApplicationController
  def options
    response.headers['Accept'] = 'GET'
    head :ok
  end

  def show
    cfg = Rails.application.config.env

    presentation = GitActivityPresentation.new cfg.GITLAB_HANDLE,
                                               cfg.GITHUB_HANDLE,
                                               github_pat: cfg.GITHUB_PERSONAL_ACCESS_TOKEN

    @activities = presentation.retrieve_spliced_activites
  end
end
