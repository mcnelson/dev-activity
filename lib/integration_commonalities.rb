module IntegrationCommonalities
  CACHE_EXPIRY = 1.day

  def cache_wrapper(key_suffix)
    klass_key = self.class.to_s.underscore

    Rails.cache.fetch("#{klass_key}-#{key_suffix}", expires_in: CACHE_EXPIRY) do
      Rails.logger.warn("#{klass_key} cache miss")
      fetch
    end
  end

  def class_name_first_segment
    self.class.to_s.match(/\A[A-Z][a-z]+/).to_s.downcase
  end
end
