module InitializedByHash
  def initialize(attributes)
    attributes.keys.detect do |k|
      raise ArgumentError, "#{self.class} does not respond to `#{k}`" unless respond_to?(k)
    end

    attributes.each { |k, v| instance_variable_set("@#{k}", v) }
  end
end
