Rails.application.routes.draw do
  resource :tweet, only: :show do
    match '', action: :options, via: :options
  end

  resource :git_activities, only: :show do
    match '', action: :options, via: :options
  end
end
