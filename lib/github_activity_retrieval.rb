require 'net/http'

class GithubActivityRetrieval
  MAX_REPO_REQUESTS = 10

  include IntegrationCommonalities
  attr_reader :handle, :personal_access_token, :json, :repos

  def initialize(handle, personal_access_token = nil)
    @handle = handle
    @personal_access_token = personal_access_token
  end

  def fetch
    fetch_events

    threads = repo_names.map { |n| Thread.new { fetch_repo_details(n) } }
    attribute_hashes = threads.map(&:value)
    @repos = attribute_hashes.reject { |r| r.has_key?('message') }

    repos.map do |repo|
      events = repo_ids_into_events.fetch(repo.fetch('full_name'))
      newest_event = events.map { |e| e.fetch('created_at') }.sort.last

      GitActivity.new(
        repo.
          slice('id', 'description', 'private').
          merge('activity_count' => events.length,
                'integration' => class_name_first_segment,
                'name' => repo.fetch('full_name'),
                'long_name' => repo.fetch('name'),
                'url' => repo.fetch('html_url'),
                'latest_activity' => newest_event,
                'obscure_description' => obscure_description(repo))
      )
    end
  end

  def fetch_with_cache
    cache_wrapper(handle) { fetch }
  end

  private

  def fetch_events
    events = []
    i = 1

    begin
      json_body = make_request "https://api.github.com/users/#{handle}/events?page=#{i}"
      page_events = JSON.parse(json_body)
      events += page_events
      i += 1
    end while i <= 2 && page_events.present?

    @json = events
  end

  def make_request(uri)
    uri = URI(uri)
    http = Net::HTTP.new(uri.hostname, uri.port)
    http.use_ssl = true

    response = nil
    http.start do |h|
      req = Net::HTTP::Get.new uri
      add_basic_auth_if_present(req) # TODO: TEST ME
      response = h.request req

      if response.is_a?(Net::HTTPNotFound)
        raise "received 404 when fetching events, does user Github '#{handle}' exist?"
      elsif !response.is_a?(Net::HTTPSuccess)
        raise "received #{response.inspect} when fetching Github user's '#{handle}' events"
      end
    end

    response.body
  end

  def add_basic_auth_if_present(request)
    return if personal_access_token.blank?
    request.basic_auth(handle, personal_access_token)
  end

  def repo_names
    repo_ids_into_events.keys[0...MAX_REPO_REQUESTS]
  end

  def fetch_repo_details(name)
    json_body = make_request "https://api.github.com/repos/#{name}"
    JSON.parse(json_body)
  end

  def repo_ids_into_events
    json.group_by { |o| o.fetch('repo').fetch('name') }
  end

  def obscure_description(repo)
    return unless repo.fetch('private')

    ["A repository with ",
     repo.fetch('open_issues_count'),
     " ",
     "open issue".pluralize(repo.fetch('open_issues_count')),
     " and ",
     repo.fetch('watchers_count'),
     " ",
     "watcher".pluralize(repo.fetch('watchers_count')),
     "."].join
  end
end
