# DevActivity

Simple API-only Rails app that retrieves your latest developer activity and tweet. Ever wanted a simple way to show what you've been working on lately? This provides the data to do something like this:

![Sample of contribution list](sample.png)

DevActivity splices together the latest events from your Github and Gitlab feeds, returning the basics about each unique project, ordered by the highest event count per each.

### Endpoints

Example on Heroku: `curl ctrb.herokuapp.com/git_activities`
```json
GET /git-activities

[
  {
    "id": 145017539,
    "description": null,
    "serviceName": "github",
    "projectName": "username/bar",
    "projectLongName": "bar",
    "projectUrl": "https://github.com/username/bar",
    "avatarUrl": null,
    "activityCount": 31,
    "latestActivityAt": "2018-08-24T20:00:14Z",
    "private": false
  },
  {
    "id": 7968828,
    "description": "A little love place where we can get together.",
    "serviceName": "gitlab",
    "projectName": "username/love-shack",
    "projectLongName": "love-shack",
    "projectUrl": "https://gitlab.com/username/love-shack",
    "avatarUrl": null,
    "activityCount": 29,
    "latestActivityAt": "2018-08-24T00:08:45.362Z",
    "private": null
  },
  {
    "id": 43987062,
    "description": "A repository with 7 open issues and 1 watcher.",
    "serviceName": "github",
    "projectName": "(private)",
    "projectLongName": "(private)",
    "projectUrl": null,
    "avatarUrl": null,
    "activityCount": 21,
    "latestActivityAt": "2018-08-24T22:43:11Z",
    "private": true
  },
  {
    "id": 117000006,
    "description": "A repository with 4 open issues and 1 watcher.",
    "serviceName": "github",
    "projectName": "(private)",
    "projectLongName": "(private)",
    "projectUrl": null,
    "avatarUrl": null,
    "activityCount": 8,
    "latestActivityAt": "2018-08-24T22:10:37Z",
    "private": true
  },
]
```

Example on Heroku: `curl ctrb.herokuapp.com/tweet`
```json
GET /tweet

{"text":"Need vim-es6 + vim-vue","createdAt":"2018-08-20T20:18:31+00:00"}
```

### How to setup

1. Deploy this codebase somewhere. Easiest place may be Heroku.

1. Set these environment variables:

    ```
    export TWITTER_HANDLE=your-handle
    export GITHUB_HANDLE=your-github-username
    export GITLAB_HANDLE=your-gitlab-username
    ```

1. If you want to show masked private activity in your feed, [get a token](https://github.com/settings/tokens) and add:

    ```
    export GITHUB_PERSONAL_ACCESS_TOKEN=asdf1234
    ```

1. Verify it's working via
    ```
    $ curl http://wherever-you-deployed.com/tweet
    ```

1. Consume the data as you wish. Vue? Angular? Ember? JQuery?

### Deployment

1. `cp config/deploy.rb.example config/deploy.rb`

1. Tweak anything you want to in `deploy.rb`.

1. `DOMAIN=wherever.you.want mina setup`

1. `DOMAIN=wherever.you.want mina deploy`

### Development

- As this app is supposed to be minimal and simple, the test suite is very basic and does not cover edge cases. Any unexpected HTTP responses are generally swallowed, such as a 301, making the returned JSON pretty fault-tolerant.

- Run the test suite with `bin/m`.

- If you encounter issues, match the environment variables you have in `.env` with those in your production environment, and curl against your local server.
